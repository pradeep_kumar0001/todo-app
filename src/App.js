import React,{useState, useEffect} from 'react';
import Header from './Component/Header';
import {Card,Container} from '@material-ui/core'
import Form from "./Component/Form";
import Todolist from './Component/Todolist'
import './App.css'

function App() {

  const initialState = JSON.parse(localStorage.getItem("todos")) || [];
  const[input, setInput] = useState("");
  const[todos, setTodos] = useState(initialState);
  const[editTodo, setEditTodo] = useState(null);
  const [filter, setFilter] = useState(() => "all");

  useEffect(()=>{
    localStorage.setItem("todos", JSON.stringify(todos));
  })


  return (
    <Container className="container">
      <Card className='app-wrapper'>
        <div>
        <Header/>
        </div>
        <div>
          <Form
            input={input}
            setInput={setInput}
            todos={todos}
            setTodos={setTodos}
            editTodo={editTodo}
            setEditTodo={setEditTodo}
            filter={filter}
            setFilter={setFilter}
          />
        </div>
        <div>
          <Todolist
          todos={todos} 
          setTodos={setTodos} 
          setEditTodo={setEditTodo}
          filter={filter}
           />
        </div>
      </Card>
    </Container>
  )
}

export default App
