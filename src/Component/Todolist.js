import React,{useState, useEffect} from 'react';
import {Button, TextField} from "@material-ui/core"
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';

function Todolist({todos, setTodos, setEditTodo, filter}) {
    const [toDosToShow, setToDosToShow] = useState(() => []);
    const[disable, setDisable] = useState(false);

    useEffect(() => {
      if (filter === "all") setToDosToShow(todos);
      else
        setToDosToShow(() => {
          let dum = [];
          if (filter === "complete") dum = todos.filter((todo) => todo.completed);
          else if (filter === "uncomplete")
            dum = todos.filter((todo) => !todo.completed);
          return dum;
        });
    });

    const handleComplete = (todo)=>{
        setTodos(
            todos.map((item)=>{
                if(item.id === todo.id){
                    return{...item, completed: !item.completed};
                }
                return item;
            })
        );
    };

   const handleEdit =({id})=>{
       const findTodo = todos.find((todo)=> todo.id ===id);
       setEditTodo(findTodo)
       
   }


    const handleDelete = ({id})=>{
        setTodos(todos.filter((todo)=> todo.id !== id));
    };

    return (
        <div className="todos-container">
            {toDosToShow.map((todo)=>(
                <li className={`list-item ${todo.completed ? "complete" : ""}`} key={todo.id}>
                    <TextField type='text'
                     value={todo.title} 
                     className={`list ${todo.completed ? "complete" : "" }`}
                     onChange={(event)=> event.preventDefault()}     
                     />
                     <div>
                         <Button className="button-complete task-button"  onClick={()=> handleComplete(todo)}>
                         <CheckBoxIcon/>
                         </Button>
                         <Button className="button-edit task-button" disabled={todo.completed ? true : false } onClick={()=> handleEdit(todo)}>
                         <EditIcon/>
                         </Button>
                         <Button className="button-delete task-button" onClick={()=> handleDelete(todo)}>
                         <DeleteIcon/>
                         </Button>
                     </div>
                </li>
            ))}
            
        </div>
    )
}

export default Todolist
