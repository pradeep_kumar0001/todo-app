import React, {useEffect} from 'react'
import {v4 as uuidv4} from "uuid"
import { Button,TextField,FormControl,Select,MenuItem } from '@material-ui/core';

export default function Form({input, setInput, todos, setTodos, editTodo, setEditTodo,filter,
    setFilter}) {
    const updateTodo = (title,id,completed)=>{
        const newTodo = todos.map((todo)=>
            todo.id === id ? {title, id, completed} : todo
        );
        setTodos(newTodo);
        setEditTodo("");
    };
    useEffect(()=>{
        if(editTodo){
            setInput(editTodo.title);
        } else{
            setInput("");
        }
    }, [setInput, editTodo]);

    const onInputChange= (event)=>{
        setInput(event.target.value);
    };

    const onChangeHandler = (e) => {
        setFilter(e.target.value);
      };

    const onFormSubmit = (event)=>{
        event.preventDefault();
        if(!editTodo)
        {
            setTodos([...todos, {id: uuidv4(), title:input, completed:false}]);
        setInput("");
        }else{
            updateTodo(input, editTodo.id, editTodo.completed)
        }
    };

    return (
        <form onSubmit={onFormSubmit} >
        <TextField id="outlined-basic" label="Enter your Task" variant="outlined" 
            type='text'
             className='task-input'
             value={input}
             onChange={onInputChange}
             required
        />
              <Button variant="outlined" className='button-add' type='submit'>{editTodo ? "Ok" : "ADD"}</Button>
              <FormControl className='filter'>
  <Select
    onChange={onChangeHandler}
    value={filter}
    style={{ padding: "10px", fontSize: "18px", marginLeft: "10px" }}
    labelId="demo-simple-select-label"
    id="demo-simple-select"
    label="All"
  >
    <MenuItem value='complete'>Completed Tasks</MenuItem>
    <MenuItem value='uncomplete'>UnComplete Tasks</MenuItem>
    <MenuItem value='all'>All Tasks</MenuItem>
    </Select>
    </FormControl>
        </form>
    )
}
