import React from 'react';
import {Typography} from '@material-ui/core'


function Header() {
    return (
        <div className="header">
            <Typography variant="h1" component="h2" className='header-title'>Todo App</Typography>
        </div>
    )
}

export default Header
